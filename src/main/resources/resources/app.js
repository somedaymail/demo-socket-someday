var stompClient = null;

function setConnected(connected) {
    $("#connect").prop("disabled", connected);
    $("#disconnect").prop("disabled", !connected);
    if (connected) {
        $("#conversation").show();
    }
    else {
        $("#conversation").hide();
    }
    $("#greetings").html("");
}
var uname;
function connect() {


    var socket = new SockJS('/lubanws');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function (frame) {
        setConnected(true);
        console.log('Connected: ' + frame);//连接成功

        //订阅：subscribe
        // 接收服务端返回的消息//@sendto
        stompClient.subscribe('/topic/message', function (data) {
            console.log("====" + data)  //server ===== success
            // showGreeting(JSON.parse(greeting.body).content);
        });
         uname = $("#uname").val(); //获取用户名
        alert(uname);
         stompClient.subscribe('/user/' + uname + '/ptp', function (data) {
            console.log("====" + data)  //server ===== success
            // showGreeting(JSON.parse(greeting.body).content);
        });

    });
}

function disconnect() {
    if (stompClient !== null) {
        stompClient.disconnect();
    }
    setConnected(false);
    console.log("Disconnected");
}

function sendName() {
    // stompClient.send("/luban/chat", {}, "app.js send success"); //广播模式@sendto
    stompClient.send("/luban/chat", {}, uname + "-app.js send success"); //点对点发送消息

    // stompClient.send("/luban/char", {}, JSON.stringify({'name': $("#name").val()}));
    //  stompClient.subscribe('/app/topic6/greetings', function (greeting) {
    //      showGreeting(JSON.parse(greeting.body).content + "呵呵");
    //  });
}

function showGreeting(message) {
    $("#greetings").append("<tr><td>" + message + "</td></tr>");
}

$(function () {
    $("form").on('submit', function (e) {
        e.preventDefault();
    });
    $("#connect").click(function () {
        connect();
    });
    $("#disconnect").click(function () {
        disconnect();
    });
    $("#send").click(function () {
        sendName();
    });
    $("#register").click(function () {


    });
});

