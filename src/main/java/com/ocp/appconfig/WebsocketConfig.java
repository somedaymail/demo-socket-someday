package com.ocp.appconfig;

import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;

/**
 * @author Administrator
 * @date 2019/9/21 0021 1:58
 * @version V1.0.0
 * 最主要重写  registerStompEndpoints
 * configureMessageBroker这两个方法
 *
 * STOMP是指消息代理或者消息协议。遵循websocket协议，并拓展了websocket协议
 * 发一个请求，通过请求头来区分，类似于mq中间件：   /app    /topic
 * 。分为两个属性：请求头和消息体分开。
 */

@Configuration
@EnableWebSocketMessageBroker
public class WebsocketConfig implements WebSocketMessageBrokerConfigurer {

    /**
     * 这方法类似于注解@ServerEndpoint（"/lubanws"）
     * @param registry
     */
    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {
       //还可以添加过滤域名
        registry.addEndpoint("/lubanws").withSockJS();

    }

    @Override
    public void configureMessageBroker(MessageBrokerRegistry registry) {
       //允许订阅的请求路径 ，点对点默认需要订阅"/user/",因为服务端发送消息给客户端的时候需要拼接格式:/user/luban/ptp
        registry.enableSimpleBroker("/topic/","/user/");
        //设置请求头
        registry.setApplicationDestinationPrefixes("/luban");

    }
}
