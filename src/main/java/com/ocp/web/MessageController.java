package com.ocp.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Administrator
 * @version V1.0.0
 * @date 2019/9/21 0021 2:06
 */
@RestController
public class MessageController {

    @Autowired
    SimpMessagingTemplate template;//模拟服务端向客户端发送消息


    @MessageMapping("/chat")

    //点对点发送需要注释  @SendTo("/topic/message")
   // @SendTo("/topic/message")  //发给所有人 ，注意路径必须是/开头，不能省略

    /**
     * Message 会把请求头和内容分开，这样便于处理
     * message 是指客户端发过来的，return 是返回给客户端的，供前端展示。
     * 注意：客户端何服务端概念是相对的。
     */
    public String broadCast(Message message) {
        byte[] bytes = (byte[]) message.getPayload();//消息内容
        String content = new String(bytes);
        System.out.println("调用成功 ：" + content);
        content = content.split("-")[0];
        serverSendMessageToClientToOne(content);
        return "server ===== success";
    }

    /**
     * 服务端向客户端发送消息
     *
     * @return 一个浏览器访问http://localhost:8080/serverSend 发送
     * 另一个浏览器访问：http://localhost:8080/index.html 这里能接受到
     * 消息，在console里面
     */
    @GetMapping("/serverSend")
    public String serverSendMessageToClient() {
        template.convertAndSend("/topic/message",
                "TASK CONPLETE !!!");

        return "serverSendMessageToClient success";
    }

    /**
     * 点对点发送，一对一发送 :convertAndSendToUser
     * 底层其实调用了		convertAndSend ，只是把路径拼接了一下 这样的格式："/user/zilu/ptp"
     * super.convertAndSend(this.destinationPrefix + user + destination, payload, headers, postProcessor);
     *
     * @return
     */
    @GetMapping("/serverSendToOne")
    public String serverSendMessageToClientToOne(String name) {
        template.convertAndSendToUser(name, "/ptp",
                "serverSendToOne TASK CONPLETE !!!");

        return "serverSendToOne success";
    }

}
